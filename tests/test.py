import sys
import os
import pytest

sys.path.append(".")
sys.path.append('D:\\Python\\pythonProject\\GreenAtom\\application')

from typing import Generator, List
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer
from application.main import app
# import asyncio
# from application.models.inbox import Inbox


@pytest.fixture(scope="module")
def client() -> Generator:
    initializer(["application.models.inbox"])
    with TestClient(app) as c:
        yield c
    finalizer()

# Попытка создания общего eventloop, доделать, если будет время
# @pytest.fixture(scope="module")
# def event_loop(client: TestClient) -> Generator:
#     yield client.task.running()


def test_post_image(client: TestClient):
    # Создаем записи в таблице в request_code = 2
    files = [
        ('image_list', ("test1.jpg", open(os.getcwd() + "\\tests\\test1.jpg", "rb"), "image/jpeg")),
        ('image_list', ("test2.jpg", open(os.getcwd() + "\\tests\\test2.jpg", "rb"), "image/jpeg")),
    ]
    response = client.post("/frame/", files=files)
    assert response.text == "null"
    assert response.status_code == 201

    # если созданы, тест будет пройден
    response = client.get("/frame/2")
    assert len(response.json()["__root__"]) == 2

    # Если сделал eventloop -> раскомментируй!
    # Не забыть про зависимости :)

    # async def get_images_by_code(request_code: int) -> List:
    #     image_list = await Inbox.all().filter(request_code=request_code)
    #     return image_list
    #
    # posted_image_list = await get_images_by_code(2)
    # assert len(posted_image_list) == 2


def test_post_not_image(client: TestClient):
    response1 = client.post("/frame/", json={"username": "admin"})
    assert response1.status_code == 422

    files = [
        ('image_list', ("test1.jpg", open(os.getcwd() + "\\tests\\test1.jpg", "rb"), "text/plain")),
    ]

    response2 = client.post("/frame/", files=files)
    assert response2.status_code == 422
    assert response2.json()["detail"] == "No images were given"


def test_get_images(client: TestClient):
    response = client.get("/frame/2")
    assert response.status_code == 200
    assert len(response.json()["__root__"]) == 2


def test_get_images_wrong_code(client: TestClient):
    response = client.get("/frame/a")
    assert response.status_code == 422


def test_delete_images(client: TestClient):
    response = client.delete("/frame/2")
    assert response.status_code == 200


def test_delete_images_wrong_code(client: TestClient):
    response1 = client.delete("/frame/a")
    assert response1.status_code == 422

    response1 = client.delete("/frame/100")
    assert response1.status_code == 404


def test_spoiled_link(client: TestClient):
    response1 = client.get("/frame/100/1")
    assert response1.status_code == 404
