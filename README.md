# GreenAtom
## Создание окружения
Загрузите зависимости:
`pip install requirements.txt`
Заполните `.env` файл
## Тонкий момент
Для корректной работы есть два варианта развертывания:
- запустить скрипт Makefile для прохождения тестирования
`make test`
- если не хотите проходить тесты до начала работы, требуется запустить скрипт для создания первой записи в бд
`python application\scripts\initial_image.py`

## Запуск 
`python application\main.py`

## Тестирование
- `make test`
- `pytest tests\test.py`
