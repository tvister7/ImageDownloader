from tortoise.contrib.pydantic import pydantic_model_creator, pydantic_queryset_creator
from application.models.inbox import Inbox

Inbox_Pydantic = pydantic_model_creator(Inbox, name="Inbox")
InboxIn_Pydantic = pydantic_model_creator(Inbox, name="InboxIn", exclude_readonly=True)
Inbox_Pydantic_List = pydantic_queryset_creator(Inbox)
