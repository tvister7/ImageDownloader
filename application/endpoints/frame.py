from typing import List
from fastapi import APIRouter, UploadFile, File
from application.crud.file_crud import get_images_by_code
from application.utilities.file_functions import create_files, delete_files

router = APIRouter()


@router.post("/", status_code=201)
async def posted_images(image_list: List[UploadFile] = File(...)):
    await create_files(image_list)


@router.get("/{request_code}")
async def get_images(request_code: int):
    return await get_images_by_code(request_code)


@router.delete("/{request_code}")
async def delete_images(request_code: int):
    await delete_files(request_code)
