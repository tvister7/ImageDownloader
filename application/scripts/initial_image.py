import asyncio
import os
import uuid
import datetime
import asyncpg
from dotenv import load_dotenv

initial_file = {"response_code": 1,
                "filename": "initial",
                "created_at": datetime.datetime.now()}

uuid.uuid4()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
load_dotenv(os.path.join(BASE_DIR, ".env"))

DATABASE_USER = os.environ["DATABASE_USER"]
DATABASE_PASSWORD = os.environ["DATABASE_PASSWORD"]
DATABASE_HOST = os.environ["DATABASE_HOST"]
DATABASE_NAME = os.environ["DATABASE_NAME"]
STATEMENT = "INSERT INTO inbox (request_code, filename, created_at) VALUES ($1, $2, $3)"


async def initial_operation():
    conn = await asyncpg.connect(user=DATABASE_USER, password=DATABASE_PASSWORD,
                                 database=DATABASE_NAME, host=DATABASE_HOST)
    await conn.execute(STATEMENT,
                       initial_file["response_code"],
                       initial_file["filename"],
                       initial_file["created_at"])

    await conn.close()


asyncio.run(initial_operation())
