from tortoise.fields import IntField, CharField, DatetimeField
from tortoise.models import Model


class Inbox(Model):
    id = IntField(pk=True)
    request_code = IntField(required=True)
    filename = CharField(max_length=41)
    created_at = DatetimeField(auto_now_add=True)
