import os
import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from application.database import init_db
from endpoints import hello, frame


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
load_dotenv(os.path.join(BASE_DIR, ".env"))


app = FastAPI(title="Image manager")


@app.on_event("startup")
async def startup_event():
    init_db(app)


app.include_router(hello.router, prefix="/hello", tags=["hello"])
app.include_router(frame.router, prefix="/frame")

HOST = os.environ["UVICORN_HOST"]
PORT = int(os.environ["UVICORN_PORT"])

if __name__ == "__main__":
    uvicorn.run("main:app", port=PORT, host=HOST, reload=True)
