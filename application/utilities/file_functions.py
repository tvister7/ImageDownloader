import os
import uuid
import arrow
from typing import List
from fastapi import UploadFile, HTTPException
from application.crud.file_crud import get_last_file, create_inboxes, delete_images_by_code

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


async def get_current_request_code() -> int:
    last_file = await get_last_file()
    if last_file:
        return last_file.dict()["request_code"]
    else:
        return 1


async def create_upload_file(uploaded_file: UploadFile):
    utc = arrow.now()
    new_filename = str(uuid.uuid4()) + ".jpeg"
    new_dirname = utc.format("YYYYMMDD")
    if not os.path.exists(os.path.join(BASE_DIR, "data", new_dirname)):
        os.mkdir(os.path.join(BASE_DIR, "data", new_dirname))
    file_location = os.path.join(BASE_DIR, "data", new_dirname, new_filename)
    with open(file_location, "wb") as file_object:
        file_object.write(uploaded_file.file.read())
    return new_filename


async def create_files(file_list: List[UploadFile]) -> None:
    file_dict = []
    if len(file_list) not in range(1, 16):
        raise HTTPException(422, detail="Wrong number of files")
    request_code = await get_current_request_code()
    for file in file_list:
        if file.content_type == "image/jpeg":
            filename = await create_upload_file(file)
            file_dict.append({"request_code": request_code + 1,
                              "filename": filename})
    if len(file_dict) == 0:
        raise HTTPException(422, detail="No images were given")
    await create_inboxes(file_dict)


async def delete_files(request_code: int) -> None:
    files_to_delete = await delete_images_by_code(request_code)
    for file in files_to_delete:
        dir_name = arrow.get(file["created_at"]).to('Europe/Moscow').format("YYYYMMDD")
        dir_path = os.path.join(BASE_DIR, "data", dir_name)
        full_filepath = os.path.join(BASE_DIR, "data", dir_path, file["filename"])
        if os.path.exists(full_filepath):
            os.remove(full_filepath)
        if not os.listdir(dir_path):
            os.rmdir(dir_path)
