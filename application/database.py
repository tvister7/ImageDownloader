import os
from dotenv import load_dotenv
from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
load_dotenv(os.path.join(BASE_DIR, ".env"))
TORTOISE_DATABASE_URL = os.environ["DATABASE_URL"]


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=TORTOISE_DATABASE_URL,
        modules={"models": ["application.models.inbox"]},
        generate_schemas=True,
        add_exception_handlers=True
    )
