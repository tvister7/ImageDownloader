from typing import List
from fastapi import HTTPException
from tortoise.exceptions import BaseORMException
from application.models.inbox import Inbox
from application.schemas.inbox_schema import Inbox_Pydantic, Inbox_Pydantic_List, InboxIn_Pydantic


async def get_last_file() -> Inbox_Pydantic:
    file = Inbox.all().order_by("-request_code").first()
    return await InboxIn_Pydantic.from_queryset_single(file)


async def create_inboxes(files_dict: List[dict]) -> None:
    for file in files_dict:
        try:
            await Inbox.create(**file)
        except BaseORMException:
            raise HTTPException(status_code=400, detail="Database error")


async def get_images_by_code(request_code: int) -> dict:
    pydantic_list = await Inbox_Pydantic_List.from_queryset(Inbox.all().filter(request_code=request_code))
    return pydantic_list.dict()


async def create_fake_inbox(inbox: InboxIn_Pydantic):
    await Inbox.create(request_code=inbox.request_code,
                       filename=inbox.filename)


async def delete_images_by_code(code: int) -> List:
    images_to_delete_obj = Inbox.filter(request_code=code).all()
    if not images_to_delete_obj:
        raise HTTPException(status_code=404, detail='No images responded to this code')
    # Data to delete real file
    images_data = await Inbox_Pydantic_List.from_queryset(images_to_delete_obj)
    datus = images_data.dict()["__root__"]
    data = []
    for element in datus:
        element.pop("id")
        element.pop("request_code")
        data.append(element)

    async for image in images_to_delete_obj:
        await image.delete()
    if not data:
        raise HTTPException(status_code=404, detail='No images responded to this code')
    return data
